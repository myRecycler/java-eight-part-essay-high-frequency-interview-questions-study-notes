# Java 八股文高频面试题学习笔记

### Keep In Mind
<div style="font-family: 楷体; color: red; font-size: 150%; font-weight: 900; ">
    要么学历牛逼，要么技术牛逼，要么都牛逼！
</div>
<div style="font-family: 楷体; color: red; font-size: 150%; font-weight: 900; ">
    如果学历无法改变，请让技术牛逼，其他都是扯淡！
</div>

#### Description
<p>Java 八股文高频面试题学习笔记</p>
<p>黑马程序员：新版Java面试专题视频教程，java八股文面试全套真题+深度详解（含大厂高频面试真题）</p>

[java八股文面试全套真题+深度详解](https://www.bilibili.com/video/BV1yT411H7YK?p=1&vd_source=67d4ec2d4c63a6f556cae895ce599b2f)：https://www.bilibili.com/video/BV1yT411H7YK?p=1&vd_source=67d4ec2d4c63a6f556cae895ce599b2f

#### Interview Mentality
强哥「高启强」推荐的《孙子兵法》中有这样一段话送给大家：<font color="red">求其上，得其中；求其中，得其下；求其下，必败！</font>
具体解释如下：
1. 如果你想进中厂，就要做进大厂的准备；
2. 如果你想找到月薪1W+的工作，就需要做月薪1W5+的准备；
3. 如果你的目标就是找到工作，起码要做冲击中小厂的准备；
4. 如果你的目标只是找个小公司混日子，大概率找不到工作。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
